from countrycode import countrycode
import requests
import random
from itertools import groupby


class GeolocationToIp():
    def __init__(self, country, city='', lat='', lng=''):
        self.country = country
        self.city = city
        self.lat = lat
        self.lng = lng
        self.ranges = []
        self.get_ranges()

    def get_ranges(self):
        return False

    def get_random_ranges(self, num=1, max_ips=0):
        return random.choice(self.ranges)

    @staticmethod
    def get_cidr(cidr_range):
        return cidr_range.split('/')[1]

    @staticmethod
    def get_cidr_count(cidr_range):
        return 2**(32-int(GeolocationToIp.get_cidr(cidr_range)))

class IPDenyGeolocationToIP(GeolocationToIp):
    def get_ranges(self):
        code = countrycode.countrycode(codes=[self.country], origin='country_name', target='iso2c')[0]
        resp = requests.get('http://www.ipdeny.com/ipblocks/data/aggregated/{}-aggregated.zone'.format(code.lower()))
        if 'title' in resp.text:
            self.ranges = []
            return False
        self.ranges = [r for r in resp.text.split('\n') if r.strip()]
        return True

    def get_random_ranges(self, num=1, max_ips=0):
        # groups = groupby(self.ranges, self.get_cidr)
        # g = []
        # keys = {}
        # for cidr, grouplist in groups:
        #     if not cidr in keys:
        #         keys[cidr] = []
        #     keys[cidr] += list(grouplist)

        if max_ips:
            rranges = []
            ips = 0
            tries = 10
            while ips < max_ips:
                r = random.choice(self.ranges)
                if not r.strip():
                    continue
                rcidr = r.split('/')[1]
                count = 2**(32-int(rcidr))
                if ips+count > max_ips+(max_ips/10):
                    if tries:
                        tries -= 1
                        continue
                    else:
                        break

                rranges.append(r)
                ips += count
        else:
            rranges = [random.choice(self.ranges)]
        return rranges


# country = input('Enter country name (defaut United States): ') or 'United States'
# city = input('Enter city name (default none): ')
# count = input('Maximum IPs (default 100000): ') or 100000

# locator = IPDenyGeolocationToIP(country, city)

# range_list = locator.get_random_ranges(max_ips=int(count))

# total_count = 0
# for cidr in range_list:
#     count = GeolocationToIp.get_cidr_count(cidr)
#     total_count += count

# print('\n'.join(range_list))
# print(total_count)