# dahua-scanner

- can search Dahua DVR, NVR, IPC in the IP address ranges with Masscan
- brute every device with credentials from `combinations.txt` or from `logins.txt` and `passwords.txt`
- make snapshots and save to `snapshots` folder
- export successfully bruted to SmartPss-acceptable XML

## Requirements
- Python 3 - install
- Python modules: `pip install -r requirements.txt`
- masscan - [build](https://github.com/robertdavidgraham/masscan) or [download](https://mega.nz/#!R5tXlAZa!IihDrWMrCrXXjZz9nTkyBFDWi3OrClw70Bp_kSn9xv4) and extract in program folder for Windows (also install WinPcap)
- Smart PSS - [download](http://www.safemag.ru/smart-pss/l)

### Usage

- write IP ranges to `scan.txt`
- specify combinations of logins and passwords in `combinations.txt` (default brute mode cause Dahua cams have block after 5 login attempts)
- to run full brute specify argument `-l` and logins and passwords in files `logins.txt` and `passwords.txt`
- to run with Masscan specify arguments `-m` and `-s`: `./dahua-scanner.py -m -s scan.txt`
- to run brute by file with IP and snapshot making only specify `-b`: `./dahua-scanner.py -b ips.txt`
- import *.xml to Smart Pss